# django.urls - provides tools for working with URL's in a django project.
# path - used to define URL patterns.
# from todolist import views
# '.' - represents the current package or directory.
from django.urls import path
from . import views

# list of URL patterns that map URL paths to view functions or classes.
urlpatterns = [
    # path() - takes route, a view, and an optional name
    # name = "index" - used to name the URL pattern
    path("", views.index, name="index"),
    # path("bio/<username>/", views.bio, name="bio"),
    # path("articles/<slug:title>/", views.article, name="article-detail"),
    # path("articles/<slug:title>/<int:section>/", views.section, name="article-section"),
    # path("blog/", include("blog.urls")),
    # ...,
]
