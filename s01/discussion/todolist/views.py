# A view function, or view for short, is a Python function that takes a web request and returns a web response.
# django.http - provides classes and functions for working with HTTP-related tasks.
# HttpResponse - class used to generate HTTP responses that are sent back to the client's browser when a view is called.
from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

# def current_datetime(request):
#     now = datetime.datetime.now()
#     html = "<html><body>It is now %s.</body></html>" % now
#     return HttpResponse(html)

# A view function takes a request and returns an HTTP response.
def index(request):
    # Generate an HTTP response with the message "Hello from the views.py file."
    return HttpResponse("Hello from the views.py file.")
